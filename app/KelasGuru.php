<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class KelasGuru extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_kelas', 'id_guru'
    ];

    public function Guru() {

        return $this->belongsToMany(Guru::class, 'kelas_guru');

    }
    public function Kelas() {

        return $this->belongsToMany(Kelas::class, 'kelas_guru');

    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'remember_token',
    ];
}
